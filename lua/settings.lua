local v = vim.opt     -- Map the vim settings interface to the local variable 'v'

--[[ Context ]]
v.colorcolumn = '85'
v.number = true
v.relativenumber = false
v.scrolloff = 4

--[[ Colors ]]
v.syntax = 'ON'
v.termguicolors = true

--[[ Search ]]
v.ignorecase = true

--[[ Indentation ]]
v.tabstop = 4
v.expandtab = true
v.shiftwidth = 4
v.autoindent = true

--[[ Window Splitting ]]
v.splitright = true
v.splitbelow = true


