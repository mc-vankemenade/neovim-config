local g = vim.g         -- Set the vim environment variables as local variable 'g'
g.t_co = 256            -- Enable 256 colors 
g.background = "dark"   -- Set background to dark
